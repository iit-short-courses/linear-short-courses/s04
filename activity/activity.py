# 2. The number of "rows" and "columns" should be provided through user input.
from numpy import *

rows = int(input("Enter the number of rows -> "))
columns = int(input("Enter the number of columns -> "))

matrixOne = zeros((rows, columns))
matrixTwo = zeros((rows, columns))

print(f"Input the values of the first matrix")

x = range(rows)
y = range(columns)
for i in x:
    for j in y:
        matrixOne[i][j] = int(input(f"matrixOne[{i}][{j}]= "))


print(f"Input the values for Second matrix")
for i in x:
    for j in y:
        matrixTwo[i][j] = int(input(f"matrixTwo[{i}][{j}]= "))

print(f"*******Display Matrix1*******")

for i in x:
    for j in y:
        print(f"{int(matrixOne[i][j])}", end = "     ")
    print("")

print(f"*******Display Matrix2*******")

for i in x:
    for j in y:
        print(f"{int(matrixTwo[i][j])}", end = "     ")
    print("")

print("Sum of two matrix")

for i in x:
    for j in y:
        print(f"{int(matrixTwo[i][j])+ int(matrixOne[i][j])}", end = "     ")
    print("")

# 3. Initialize the variables matrixOne, matrixTwo, and matrixSum with the provided rows and columns value.
# 4. Iterate through the "rows" and "columns" for each matrix (matrixOne and matrixTwo) to get the values from the user input. Print the matrixOne and matrixTwo after getting all the values.
# 5. Iterate through the "rows" and "columns" of all the matrices to add all the values of matrixOne and matrixTwo. Store the added values in the matrixSum variable and print it in the terminal.
# 6. Update your gitlab repository named data-structures-lastName and push to git with the commit message of Add activity code s04.
# 7. Add the link in Boodle.