# Matrix======================

# install pip3 in our discussion folder
	# pip3 is the official package manager and pip command for Python 3. meaning, it is similar to the npm command in javascript.
	# curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

# install numpy
	# is fundamental package for scientific computing in Python. it is a library that provides multidimensional array object, derived objects (masked arrays and matrices), and assortment of routines for fast operations on arrays
	# pip3 install numpy


# [Section] Matrix
# a matrix represents a collection of numbers arranged in an order of rows and columns
# it is necessary to enclose the elements of a matrix in parenthesis or brackets
# it is a series of numbers in a grid format
from numpy import *

# execute the matrix without the use of numpy library
seat_1 = [ [ 2 for i in range(0) ] for j in range(3) ]
print(seat_1)
# result: [[], [], []]

# create a matrix using numpy
box = array([
	[1, 2, 3],
	[4, 5, 6],
	[7, 8, 9],
	[10, 11, 12],
])
print(box)

# [Section] Methods for Matrices
# .ndim
	# returns the number of dimensions in a multidimentional array
print(box.ndim)

# .shape
	# returns the number of columns and rows
print(box.shape)

# .size
	# returns the size of an array; 12 -> number of elements in the matrix
print(box.size)

# flatten()
	# merges the elements into one array, making it 1-dimentional array
print(box.flatten())

# reshape()
	# change the shape of the array which can be done as long as the elements required is equal to the shape that we specify
flat_box = box.flatten()
print(flat_box.reshape(2, 6))

# diagonal()
	# finds the elements in a diagonal position from a given matrix
diag_matrix = array([
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
])
print(diag_matrix)
print(diagonal(diag_matrix))

# min() vs max()
	# min() finds the minimum (least) value
	# max() finds the maximum (greatest) value
print(diag_matrix.min())
print(diag_matrix.max())

# [Section] Other ways to create Matrix

# matrix()
	# can create matrix by declaring the elements and separating the arrays with semicolons
new_matrix = matrix("11 2 3; 3 44 5; 6 7 8")
print(new_matrix)

# zeros() and ones()
	# used to get a new array of given shape and type, filled with either zeros or ones.
zero_matrix = zeros((2, 3))
# zero_matrix = zeros(2, 3) returns an error due to lack of parenthesis
one_matrix = ones((4, 5))
print(zero_matrix)
print(one_matrix)

# declaring the data type
new_matrix_2 = zeros((2, 3), dtype=string_)
print(new_matrix_2)

# [Section] Manipulating Matrix

# indexing
	# accessing the value in a matrix
	# name[row][col]
sched = array([
	['Mon', 5, 10, 2],
	['Tue', 4, 3, 1],
	['Wed', 7, 8, 9],
])
print(sched[1])
print(sched[1][2])

# append() and insert()
# append() - allows adding of rows in the matrix
	# append(arr, [[values]], axis)
		# axis values: 
			# 0 - rows
			# 1 - columns
sched=append(sched, [['Thu', 23, 12, 13]], 0)
print(sched)

# insert() - allows adding of columns in the matrix
	# insert( arr, [col_index], [[values]], axis )
sched = insert(sched, [4], [[27], [10], [3], [4]], 1)
print(sched)

# delete()
	# deleting using the index through rows and columns
		# delete(arr, [index], axis)
sched = delete(sched, [2], 0) # deletes the row that belongs to index 2
# sched = delete(sched, [2], 1) - will delete columns with index 2
print(sched)

# [Section] Operations
# elements should have the same shape and number of rows/columns to perform an operation

mat_one = array([[5, 10], [5, 5]])
mat_two = array([[5, 10], [5, 5]])

# add() subtract() divide() multiply()
print("add()")
print(add(mat_one, mat_two))

print("subtract()")
print(subtract(mat_one, mat_two))

print("divide()")
print(divide(mat_one, mat_two))

print("multiply()")
print(multiply(mat_one, mat_two))

# .dot() - another way of performing the multiply()
print("dot()")
print(mat_one.dot(mat_two))
mat_one = array([[5, 10], [5, 5]])
mat_two = array([[5, 10], [5, 5]])
# dot()
	# row 1 * col 1 (5 * 5) + (10 * 5) = 75
	# row 1 * col 2 (5 * 10) + (10 * 5) = 100
	# row 2 * col 1 (5 * 5) + (5 * 5) = 50
	# row 2 * col 2 (5 * 10) + (5 * 5) = 75
	# result: [[ 75 100]
			# [ 50  75]]

# ==without Numpy==
# addition
vip_seat = [
	[1,1,1],
	[3,3,3],
	[5,5,5],
]

seat_nos = [
	[2,2,2],
	[4,4,4],
	[6,6,6],
]

seat = [
	[0,0,0],
	[0,0,0],
	[0,0,0]
]

for i in range(len(seat_nos)):
	for k in range(len(vip_seat)):
		seat[i][k] = seat_nos[i][k]+vip_seat[i][k]

for r in seat:
	print(r)